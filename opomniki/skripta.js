window.addEventListener("load", function() {
	// Stran naložena
		
	// Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML, 10);
	
			// TODO:
			// - če je čas enak 0, izpiši opozorilo
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	};
	
	var izvediPrijavo= function(){
		var uporabnik = document.querySelector("#uporabnisko_ime").value;
		
		document.querySelector("#uporabnik").innerHTML = uporabnik;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
		
	}
		document.querySelector("#prijavniGumb").addEventListener('click',izvediPrijavo);
	
	var dodajOpomnik = function(){
		var naziv = document.querySelector("#naziv_opomnika").value;
		var casTrajanja = document.querySelector("#cas_opomnika").value;
		document.querySelector("#naziv_opomnika").value = "";
		document.querySelector("#cas_opomnika").value = "";
		var opomniki = document.querySelector("#opomniki");
		
		opomniki.innerHTML += " \
			<div class='opomnik'> \
		      <div class='naziv_opomnika'> " + naziv + "</div> \
		      <div class='cas_opomnika'> Opomnik čez <span>" + casTrajanja + "</span> sekund.</div> \
		    </div>";
	}
	
	document.querySelector("#dodajGumb").addEventListener('click', dodajOpomnik);
	

	
	setInterval(posodobiOpomnike, 1000);
	
});